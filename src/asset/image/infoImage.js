import like from '../image/like-yellow-icon.png'
import avatar from '../image/1.webp'

const image = {
    like,
    avatar
}

export {image}