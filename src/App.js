import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import ContentComponent from './component/Content/Content';
import TextHeader from './component/Header/TextHeader';
import ImageHeader from './component/Header/ImageHeader';
function App() {
  return (
    <div className='container'>
      <TextHeader/>
      <ImageHeader/>
      <ContentComponent/>
    </div>
  )
}

export default App;
