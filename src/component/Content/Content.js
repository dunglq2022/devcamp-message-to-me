import { Component } from "react";
import InputContent from "./Input";
import OutputContent from "./Output";

class ContentComponent extends Component {
    render() {
        return (
        <>
            <InputContent/>
            <OutputContent/>
        </>
        )
    }
}

export default ContentComponent;