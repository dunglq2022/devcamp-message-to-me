import { Component } from "react";
import {image} from '../../asset/image/infoImage'

class OutputContent extends Component{
    render(){
        return(
            <div className='pt-2'>
                <p className='text-primary'>Tôi vất vả quá, ông thì vui hơn</p>
                <img src={image.like} width={100} alt="anh-like"/>
            </div>
        )
    }
}
export default OutputContent;