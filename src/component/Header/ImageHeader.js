import { Component } from "react";
import {image} from '../../asset/image/infoImage'

class ImageHeader extends Component {
    render() {
        return (
            <div className='image-avatar'>
                <img src={image.avatar} alt='avatar' className="img-fluid"></img>
            </div>
        )
    }
}

export default ImageHeader